export const blue = {

  '--primary' : '#92ddea',
  '--primary-variant': '#001b48',

  '--secondary': '#97cbdc',
  '--secondary-variant': '#6f74dd',

  '--background': '#001b48',
  '--surface': '#004581',
  '--dialog': '#004581',
  '--cancel': '#018abd',
  '--alt-surface': '#004581',
  '--alt-dialog': '#004581',

  '--on-primary': '#000000',
  '--on-secondary': '#000000',
  '--on-background': '#FFFFFF',
  '--on-surface': '#FFFFFF',
  '--on-cancel': '#000000',

  '--green': 'lightgreen',
  '--red': 'red',
  '--yellow': 'yellow',
  '--blue': '#eb8b35',
  '--purple': '#9c27b0',
  '--light-green': '#80ba24',
  '--grey': '#BDBDBD',
  '--grey-light': '#9E9E9E',
  '--black': '#212121',
  '--moderator': '#004581'
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'Blue-Blue',
      'de': 'Blue-Blue'
    },
    'description': {
      'en': 'Blue everywhere for calm work environment',
      'de': 'Überall Blau für ruhiges Arbeiten'
    }
  },
  'isDark': true,
  'order': 4,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'background'

};
